<img src="https://raw.githubusercontent.com/vasanthdeveloper/mantu/designs/banner.png" alt="mantu"></br>

**mantu** is a Discord bot specially built for Vasanth Developer's Discord server, to add special features which are not achievable with existing bots.

## ⚡️ Features
1. Generates invite links & redirects from a vanity URL
2. Gracefully kicks people if they're inactive for set period
3. Pulls _Programmer Memes_ from Reddit
3. Creates resources related to events and then cleans them up later

## 📜 License
> The **mantu** project is released under the [GPL v2](LICENSE.md). <br> Developed &amp; maintained By Vasanth Srivatsa. Copyright 2021 © Vasanth Developer.
<hr>

> <a href="https://vasanth.tech" target="_blank" rel="noopener">vasanth.tech</a> &nbsp;&middot;&nbsp;
> YouTube <a href="https://vas.cx/videos" target="_blank" rel="noopener">@vasanthdeveloper</a> &nbsp;&middot;&nbsp;
> Twitter <a href="https://vas.cx/twitter" target="_blank" rel="noopener">@vasanthdevelop</a> &nbsp;&middot;&nbsp;
> Discord <a href="https://vas.cx/discord" target="_blank" rel="noopener">Vasanth Developer</a>